<%@page import="ds.gae.view.JSPSite"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<% 
	String renter = (String)session.getAttribute("renter");
	String token = (String)session.getAttribute("token");
	JSPSite currentSite = JSPSite.CONFIRM_QUOTES_RESPONSE;

%>   
 
<%@include file="_header.jsp" %>

<% 
if (currentSite != JSPSite.LOGIN && currentSite != JSPSite.PERSIST_TEST && renter == null) {
 %>
	<meta http-equiv="refresh" content="0;URL='/login.jsp'">
<% 
  request.getSession().setAttribute("lastSiteCall", currentSite);
} 
 %>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="style.css" />
	<title>Car Rental Application</title>
</head>
<body>
	<div id="mainWrapper">
		<div id="headerWrapper">
			<h1>Car Rental Application</h1>
		</div>
		<div id="navigationWrapper">
			<ul>
<% 
for (JSPSite site : JSPSite.publiclyLinkedValues()) {
	if (site == currentSite) {
 %> 
				<li><a class="selected" href="<%=site.url()%>"><%=site.label()%></a></li>
<% } else {
 %> 
				<li><a href="<%=site.url()%>"><%=site.label()%></a></li>
<% }}
 %> 

				</ul>
		</div>
		<div id="contentWrapper">
<% if (currentSite != JSPSite.LOGIN) { %>
			<div id="userProfile">
				<span>Logged-in as <%= renter %> (<a href="/login.jsp">change</a>)</span>
			</div>
<%
   }
 %>
			<div class="frameDiv" style="margin: 150px 150px;">
				<H2>Reply</H2>
				<div class="group">
					<p id=notification>
					</p>
				</div>
			</div>
			<script src='/_ah/channel/jsapi'></script>
			<script type="text/javascript">
				var token = "<%=token%>";
				var channel = new goog.appengine.Channel(token);
				onOpened = function() {
					document.getElementById("notification").innerText = "Dear <%=renter%>, Your quotes are being processed."
				}
				onMessage = function(message) {
					document.getElementById("notification").innerText = message.data;
				}
				
				var handler = {
						'onopen': onOpened,
						'onmessage': onMessage,
						'onerror': function() {},
						'onclose': function() {}
				};
				var socket = channel.open(handler);
				socket.onopen = onOpened;
				socket.onmessage = onMessage;
			</script>

<%@include file="_footer.jsp" %>

