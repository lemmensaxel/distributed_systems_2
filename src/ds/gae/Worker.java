package ds.gae;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Properties;

import javax.mail.*;
import javax.mail.internet.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.channel.ChannelMessage;
import com.google.appengine.api.channel.ChannelService;
import com.google.appengine.api.channel.ChannelServiceFactory;

import ds.gae.entities.Quote;

public class Worker extends HttpServlet {
	private static final long serialVersionUID = -7058685883212377590L;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
			InputStream payload = req.getInputStream();
			ObjectInputStream in = new ObjectInputStream(payload);
			String renter = "";
			try {
				@SuppressWarnings("unchecked")
				ArrayList<Quote> quotes = (ArrayList<Quote>) in.readObject();
				renter = quotes.get(0).getCarRenter();
				CarRentalModel.get().confirmQuotes(quotes);
				backChannel("Dear "+renter+", all your quotes have been confirmed.",renter);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (ReservationException e) {
				backChannel("Dear "+renter+", there was an error while confirming your quotes.",renter);
				e.printStackTrace();
			}
	}
	
	private void backChannel(String message, String renter) throws IOException{
		System.out.println("Sending message to :"+renter);
		ChannelService backChannel = ChannelServiceFactory.getChannelService();
		backChannel.sendMessage(new ChannelMessage(renter, message));
		this.sendEmail(message, renter);
	}
	
	private void sendEmail(String reply, String renter) {
		// Renter email
      String to = renter+"@gmail.com";
      
      String from = "adminCarRental@gmail.com";

      String host = "localhost";
      
      Properties properties = System.getProperties();
      
      properties.setProperty("mail.smtp.host", host);

      Session session = Session.getDefaultInstance(properties);

      try {
         MimeMessage message = new MimeMessage(session);

         message.setFrom(new InternetAddress(from));

         message.addRecipient(Message.RecipientType.TO , new InternetAddress(to));

         message.setSubject("Confirm Quotes");

         message.setText(reply);

         //Transport.send(message);
         System.out.println("Sent message successfully....");
      }catch (MessagingException mex) {
         mex.printStackTrace();
      }
	}
}
