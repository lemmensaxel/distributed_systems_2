package ds.gae;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import ds.gae.entities.Car;
import ds.gae.entities.CarRentalCompany;
import ds.gae.entities.CarType;
import ds.gae.entities.Quote;
import ds.gae.entities.Reservation;
import ds.gae.entities.ReservationConstraints;
 
public class CarRentalModel {	
	
	private static CarRentalModel instance;
	
	public static CarRentalModel get() {
		if (instance == null)
			instance = new CarRentalModel();
		return instance;
	}
		
	/**
	 * Get the car types available in the given car rental company.
	 *
	 * @param 	crcName
	 * 			the car rental company
	 * @return	The list of car types (i.e. name of car type), available
	 * 			in the given car rental company.
	 */
	public Set<String> getCarTypesNames(String crcName) {
		EntityManager em = EMF.get().createEntityManager();
		try {
			String query = "SELECT carType.name "+
						   "FROM CarType carType " +
						   "WHERE carType.company = :crcName";
			return new HashSet<String>(em.createQuery(query,String.class).setParameter("crcName", crcName).getResultList());
		} finally {
			em.close();
		}
	}
	
	

    /**
     * Get all registered car rental companies
     *
     * @return	the list of car rental companies
     */
    public Collection<String> getAllRentalCompanyNames() {
    	EntityManager em = EMF.get().createEntityManager();
		try {
			String query = "SELECT crc.name "+
						   "FROM CarRentalCompany crc";
			return em.createQuery(query,String.class).getResultList();
		} finally {
			em.close();
		}
    }
	
	/**
	 * Create a quote according to the given reservation constraints (tentative reservation).
	 * 
	 * @param	company
	 * 			name of the car renter company
	 * @param	renterName 
	 * 			name of the car renter 
	 * @param 	constraints
	 * 			reservation constraints for the quote
	 * @return	The newly created quote.
	 *  
	 * @throws ReservationException
	 * 			No car available that fits the given constraints.
	 */
    public Quote createQuote(String company, String renterName, ReservationConstraints constraints) throws ReservationException {
    	EntityManager em = EMF.get().createEntityManager();
		try {
			return em.find(CarRentalCompany.class,company).createQuote(constraints, renterName);
		} catch (Exception e) {
			throw new ReservationException("Car not available.");
		} finally {
			em.close();
		}
    }
    
	/**
	 * Confirm the given quote.
	 *
	 * @param 	q
	 * 			Quote to confirm
	 * 
	 * @throws ReservationException
	 * 			Confirmation of given quote failed.	
	 */
	public Reservation confirmQuote(Quote q) throws ReservationException {
		EntityManager em = EMF.get().createEntityManager();
		try {
			return em.find(CarRentalCompany.class,q.getRentalCompany()).confirmQuote(q);
		} finally {
			em.close();
		}
	}
	
    /**
	 * Confirm the given list of quotes
	 * 
	 * @param 	quotes 
	 * 			the quotes to confirm
	 * @return	The list of reservations, resulting from confirming all given quotes.
	 * 
	 * @throws 	ReservationException
	 * 			One of the quotes cannot be confirmed. 
	 * 			Therefore none of the given quotes is confirmed.
	 */
    public List<Reservation> confirmQuotes(List<Quote> quotes) throws ReservationException {    	
    	EntityManager em = EMF.get().createEntityManager();
    	EntityTransaction transaction = em.getTransaction();
		try {
			transaction.begin();
			List<Reservation> reservations = new ArrayList<Reservation>();
			for(Quote q : quotes){
				reservations.add(em.find(CarRentalCompany.class,q.getRentalCompany()).confirmQuote(q));
			}
			transaction.commit();
			return reservations;
		} catch (ReservationException e) {
			transaction.rollback();
			throw e;
		} finally {
			em.close();
		}
    }
	
	/**
	 * Get all reservations made by the given car renter.
	 *
	 * @param 	renter
	 * 			name of the car renter
	 * @return	the list of reservations of the given car renter
	 */
	public List<Reservation> getReservations(String renter) {
		EntityManager em = EMF.get().createEntityManager();
		try {
			String query = "SELECT res "+
						   "FROM Reservation res " +
						   "WHERE res.carRenter = :renter";
			return em.createQuery(query,Reservation.class).setParameter("renter", renter).getResultList();
		} finally {
			em.close();
		}
    }

    /**
     * Get the car types available in the given car rental company.
     *
     * @param 	crcName
     * 			the given car rental company
     * @return	The list of car types in the given car rental company.
     */
    public Collection<CarType> getCarTypesOfCarRentalCompany(String crcName) {
    	EntityManager em = EMF.get().createEntityManager();
		try {
			String query = "SELECT carType "+
						   "FROM CarType carType " +
						   "WHERE carType.company = :crcName";
			return em.createQuery(query,CarType.class).setParameter("crcName", crcName).getResultList();
		} finally {
			em.close();
		}
    }
	
    /**
     * Get the list of cars of the given car type in the given car rental company.
     *
     * @param	crcName
	 * 			name of the car rental company
     * @param 	carType
     * 			the given car type
     * @return	A list of car IDs of cars with the given car type.
     */
    public Collection<Integer> getCarIdsByCarType(String crcName, CarType carType) {
    	Collection<Integer> out = new ArrayList<Integer>();
    	for (Car c : getCarsByCarType(crcName, carType)) {
    		out.add(c.getId());
    	}
    	return out;
    }
    
    /**
     * Get the amount of cars of the given car type in the given car rental company.
     *
     * @param	crcName
	 * 			name of the car rental company
     * @param 	carType
     * 			the given car type
     * @return	A number, representing the amount of cars of the given car type.
     */
    public int getAmountOfCarsByCarType(String crcName, CarType carType) {
    	return this.getCarsByCarType(crcName, carType).size();
    }

	/**
	 * Get the list of cars of the given car type in the given car rental company.
	 *
	 * @param	crcName
	 * 			name of the car rental company
	 * @param 	carType
	 * 			the given car type
	 * @return	List of cars of the given car type
	 */
	private List<Car> getCarsByCarType(String crcName, CarType carType) {				
		EntityManager em = EMF.get().createEntityManager();
		try {
			String query = "SELECT carType.cars "+
					   "FROM CarType carType " +
					   "WHERE carType.company = :crcName AND carType.name = :cartype";
			List<Car> cars = (List<Car>) em.createQuery(query,List.class).setParameter("crcName", crcName).setParameter("cartype", carType.getName()).getSingleResult();
			return cars;
		} finally {
			em.close();
		}
	}

	/**
	 * Check whether the given car renter has reservations.
	 *
	 * @param 	renter
	 * 			the car renter
	 * @return	True if the number of reservations of the given car renter is higher than 0.
	 * 			False otherwise.
	 */
	public boolean hasReservations(String renter) {
		return this.getReservations(renter).size() > 0;		
	}

	public void addCarRentalCompany(CarRentalCompany company) {
		EntityManager em = EMF.get().createEntityManager();
		try {
			em.persist(company);
		} catch (Exception ex) {
			System.err.println(ex.getMessage());
		} finally {
			em.close();
		}
		
	}	
}