package ds.gae.servlets;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.appengine.api.channel.ChannelService;
import com.google.appengine.api.channel.ChannelServiceFactory;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;

import ds.gae.entities.Quote;
import ds.gae.view.JSPSite;

@SuppressWarnings("serial")
public class ConfirmQuotesServlet extends HttpServlet {
	
	@SuppressWarnings("unchecked")
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		HttpSession session = req.getSession();
		HashMap<String, ArrayList<Quote>> allQuotes = (HashMap<String, ArrayList<Quote>>) session.getAttribute("quotes");

		ArrayList<Quote> qs = new ArrayList<Quote>();
		
		for (String crcName : allQuotes.keySet()) {
			qs.addAll(allQuotes.get(crcName));
		}
		String renter = (String) session.getAttribute("renter");
		ChannelService backChannel = ChannelServiceFactory.getChannelService();
		String token = backChannel.createChannel(renter);
		session.setAttribute("token", token);
		
		Queue queue = QueueFactory.getDefaultQueue();
		queue.add(TaskOptions.Builder.withUrl("/worker").payload(serialize(qs)));
		
		session.setAttribute("quotes", new HashMap<String, ArrayList<Quote>>());
		
		resp.sendRedirect(JSPSite.CONFIRM_QUOTES_RESPONSE.url());
	}
	
	public static byte[] serialize(Object object) throws IOException {
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		ObjectOutput out = new ObjectOutputStream(b);   
		out.writeObject(object);
		out.flush();
		return b.toByteArray();
		
	}
}
